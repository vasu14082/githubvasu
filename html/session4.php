<!DOCTYPE html>
<html>
<head>
	<title>SESSION 3(PHP)</title>
</head>
<body>
<?php
$host = 'localhost';
$user = 'root';
$pass = 'root';
$dbname = 'college';
$i = 102;


$conn = mysqli_connect($host,$user,$pass,$dbname);

if($conn)
{
	echo "connect to the mysql server <br><br>";
}
else
{
	echo "not connected";
}

$query = "create table if not exists student(student_id int auto_increment primary key,name varchar(80),email varchar(100),branch varchar(100),father_name varchar(80),gender varchar(7),age int,date_of_birth DATE, created_at DATETIME,updated_at DATETIME)";



if(mysqli_query($conn,$query))				//for creating table 
{
	echo "table created succesfully <br>";
	echo "<br>";
}
else
{
	echo "failed".mysqli_query_error();	
}

$insertQuery = "insert into student (name, email, branch, father_name, gender, age, date_of_birth, created_at, updated_at) values ('vasudev','vasudev@123.com','science','AD','male',22,'1998-12-20',now(),now())";		//insert values in table 


mysqli_report(MYSQLI_REPORT_ALL);//for checking php and mysql error


try{
	if(mysqli_query($conn,$insertQuery) == false)			// for exception hadling whether query is executed or not
	{
		throw new Exception();  	// if no
		
	}
	else
	{								// if yes
		$last_id = mysqli_insert_id($conn);			//for getting last inserted id
		echo "<br><br><br>value inserted successfull <br>";		
		echo "last inserted id is <span style='font-weight: bold;'>".$last_id."<span>";
		
		
	}

}
catch(Exception $e){
	echo 'Message: ' .$e->getMessage();		//exact error message
}




?>
</body>
</html>


